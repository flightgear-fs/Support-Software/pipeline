#!groovy
/*
    Jenkins Pipeline script to build PLIB, SimGear and FlightGear
    Written May, 2017 by Torsten Dreyer - torsten (at) t3r (dot) de

    This file is part of FlightGear.

    FlightGear is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    FlightGear is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with FlightGear.  If not, see <http://www.gnu.org/licenses/>.
*/

node("linux") {
    stage( "Copy OSG") {
        step ([$class: 'CopyArtifact',
            projectName: 'OSG-Stable',
            filter: 'dist/**']);
    }

    stage( "PLIB") {
        dir('plib') {
            git branch: 'master', changelog: true, poll: true, url: 'https://git.code.sf.net/p/libplib/code'

            dir('build') {
                sh 'cmake -G "Unix Makefiles" -D CMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="$WORKSPACE/dist" -D CMAKE_CXX_FLAGS="-pipe" -D CMAKE_INSTALL_PREFIX:PATH=$WORKSPACE/dist ../'
                sh 'make'
                sh 'make install'
            }
        }
    }
    
    stage ("SimGear") {
        dir('simgear') {
            git branch: 'next', changelog: true, poll: true, url: 'https://git.code.sf.net/p/flightgear/simgear'
            
            dir('build') {
                sh 'cmake -G "Unix Makefiles" -D CMAKE_BUILD_TYPE=Release -DENABLE_RTI=OFF -DCMAKE_PREFIX_PATH="$WORKSPACE/dist" -DENABLE_DNS=ON -D CMAKE_CXX_FLAGS="-pipe" -D CMAKE_INSTALL_PREFIX:PATH=$WORKSPACE/dist ../'
                sh 'make'
                sh 'make install'
            }
        }
    }
    
    stage ("SimGear-Test") {
        dir('simgear') {
            dir('build') {
                sh 'make test'
            }
        }
    }

    stage ("FlightGear"){
        dir('flightgear') {
            git branch: 'next', changelog: true, poll: true, url: 'https://git.code.sf.net/p/flightgear/flightgear'
            
            dir('build') {
                sh 'cmake -G "Unix Makefiles" -D CMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="$WORKSPACE/dist" -DCMAKE_CXX_FLAGS="-pipe" -DCMAKE_INSTALL_PREFIX:PATH=$WORKSPACE/dist -DSP_FDMS=1 -DENABLE_LARCSIM=1 -DENABLE_UIUC_MODEL=1 -DENABLE_RTI=1 -DENABLE_FLITE=1 ../'
                sh 'make'
                sh 'make install'
            }
        }
    }

    stage ("FlightGear-Test"){
        dir('data') {
            /* git shortcut does not provide git timeout - booh */
            /* git branch: 'next', changelog: true, poll: true, url: 'git://git.code.sf.net/p/flightgear/fgdata' */
            checkout([
                $class: 'GitSCM', branches: [[name: '*/next']],
                doGenerateSubmoduleConfigurations: false,
                extensions: [
                    [$class: 'CheckoutOption', timeout: 60],
                    [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false, timeout: 60]
                ],
                submoduleCfg: [],
                userRemoteConfigs: [[url: 'git://git.code.sf.net/p/flightgear/fgdata']]])
        }
        dir('flightgear/build') {
            sh 'FG_ROOT=$WORKSPACE/data make test'
        }
    }
}

